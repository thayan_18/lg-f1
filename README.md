#  LG FI-11

## Instalação

1. Certifique-se de ter o [Node.js](https://nodejs.org) instalado.
2. Clone este repositório: `https://gitlab.cesar.org.br/lg-mao/fi-11/back-end-api.git`
3. Navegue até o diretório do projeto: `cd back-end-api`
4. Instale as dependências: `npm install`

## Configuração

Antes de executar o projeto, é necessário configurar algumas variáveis de ambiente. Siga as etapas abaixo:

1. Na raiz do seu projeto, crie um novo arquivo chamado .env
2. Abra o arquivo .env e adicione as variáveis de ambiente que você deseja configurar, seguindo o formato CHAVE=VALOR. Por exemplo:
3. As variáveis de ambiente relacionadas ao banco de dados devem ser configuradas da seguinte forma: Certifique-se de preencher as informações corretas para as variáveis:

`DB_USERNAME`, `DB_PASSWORD` e `DB_DATABASE`.

## Uso

### Ambiente de Desenvolvimento

Para executar o projeto no ambiente de desenvolvimento, use o seguinte comando:

```bash
npm run dev

http://localhost:3000
http://localhost:3000/api-docs/#/
```  

### Ambiente de Testes

Para executar os testes, use o seguinte comando:

```bash
npm run test
```  

## Contribuição

As contribuições são bem-vindas! Sinta-se à vontade para desenvolver novas funcionalidades, fazer correções ou melhorias no código.
