# Define a imagem base
FROM node:18.16.0

# Configura o diretório de trabalho
WORKDIR /APP-LG-FI

# Copia o package.json e o package-lock.json para o diretório de trabalho
COPY package*.json ./

# Instala as dependências
RUN npm install npm@latest -g
RUN npm install

# Copia o restante dos arquivos do aplicativo para o diretório de trabalho
COPY . .

# Comando para iniciar o aplicativo
CMD [ "npm", "start" ]
