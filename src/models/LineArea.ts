import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Factory } from './Factory';
import { User } from './User';

@Entity()
export class LineArea {

  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ nullable: false })
  public name: string;

  @ManyToOne(() => User,  { eager: true })
  @JoinColumn()
  public supervisor: User;

  @ManyToOne(() => Factory, (factory) => factory.lineAreaList, { eager: true })
  @JoinColumn()
  public factory: Factory;
}
