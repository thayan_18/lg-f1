import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { StatusAuditEnum } from '../enums/StatusAuditEnum';
import { FactoryImprovements } from './FactoryImprovements';
import { LineArea } from './LineArea';
import { Subsidiary } from './Subsidiary';
import { User } from './User';

@Entity()
export class Audit {

  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ nullable: true })
  public creationDate: Date;

  @Column({ nullable: true })
  public startDate: Date;

  @Column({ nullable: true })
  public endDate: Date;

  @Column({nullable: false})
  public status: StatusAuditEnum;

  @ManyToOne(() => Subsidiary)
  @JoinColumn()
  public subsidiary: Subsidiary;

  @ManyToOne(() => FactoryImprovements, { nullable: false })
  @JoinColumn()
  public factoryImprovements: FactoryImprovements;

  @ManyToOne(() => User, { nullable: false })
  @JoinColumn()
  public userAuditor: User;

  @ManyToOne(() => LineArea, { nullable: false, eager: true })
  @JoinColumn()
  public lineArea: LineArea;
}
