import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('users')
export class User {

  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ nullable: false })
  public name: string;

  @Column({ nullable: false, unique: true })
  public login: string;

  @Column({ nullable: false, default: false })
  public isActive: boolean;
}
