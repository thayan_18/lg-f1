import { Column, Entity, JoinColumn, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { LineArea } from './LineArea';

@Entity()
export class Factory {

  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ nullable: false })
  public name: string;

  @OneToMany(() => LineArea, (line) => line.factory)
  @JoinColumn()
  public lineAreaList: Array<LineArea>;
}
