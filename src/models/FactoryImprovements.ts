import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class FactoryImprovements {

  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ nullable: false, unique: true })
  public name: string;
}
