import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';


@Entity()
export class Subsidiary {

  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ nullable: false })
  public name: string;
}
