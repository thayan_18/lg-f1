import { User } from '../models/User';

export class AuthorizationDTO {
  public accessToken: string;
  public createdIn:string;
  public expiresIn: string;
  public user:User;
}
