import { FactoryDTO } from './FactoryDTO';
import { LineAreaDTO } from './LineAreaDTO';
import { UserDTO } from './UserDTO';


export class AuditDTO {
  public id: number;
  public status: string;
  public userAuditor: UserDTO;
  public userSupervisorLineArea: UserDTO;
  public lineArea: LineAreaDTO;
  public factory: FactoryDTO;
}
