export class AuthorizationLgDTO {
  public UserId: string;
  public UserName: string;
  public UserDomain: string;
  public IsAuthenticated: boolean;
  public RequestDate: string;
  public ResponseId: string;
  public ResponseDate: string;
}
