import { AuditDTO } from '../../dto/AuditDTO';

export const auditDTOMock1 = {
  id: 1,
  status: 'TODO',
  userAuditor: {
    id: 2,
    name: 'Richard Araujo'
  },
  userSupervisorLineArea: {
    id: 3,
    name: 'Thiago Brandão'
  },
  factory: {
    id: 1,
    name: 'TV/AV'
  }
} as unknown as AuditDTO;

export const auditDTOMock2 = {
  id: 2,
  status: 'TODO',
  userAuditor: {
    id: 2,
    name: 'Richard Araujo'
  },
  userSupervisorLineArea: {
    id: 3,
    name: 'Thiago Brandão'
  },
  factory: {
    id: 2,
    name: 'H&A'
  }
} as unknown as AuditDTO;
