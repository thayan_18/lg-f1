
// AuditRepositoryMock

import { Audit } from '../../models/Audit';

export const auditObjMock1 = {
  id: 5,
  creationDate: '2023-07-06T22: 36: 39.000Z',
  startDate: '2023-07-06T22: 36: 39.000Z',
  endDate: '2023-07-06T22: 36: 39.000Z',
  status: 'TODO',
  userAuditor: {
    id: 2,
    name: 'Richard Araujo',
    login: 'richard.araujo',
    isActive: true
  },
  lineArea: {
    id: 5,
    name: 'TV-05',
    supervisor: {
      id: 3,
      name: 'Thiago Brandão',
      login: 'thiago.brandão',
      isActive: true
    },
    factory: { id: 2, name: 'H&A' }
  }
} as unknown as Audit;

export const auditObjMock2 = {
  id: 6,
  creationDate: '2023-07-06T22:36:39.000Z',
  startDate: '2023-07-06T22:36:39.000Z',
  endDate: '2023-07-06T22:36:39.000Z',
  status: 'COMPLETED',
  userAuditor: {
    id: 2,
    name: 'Richard Araujo',
    login: 'richard.araujo',
    isActive: true
  },
  lineArea: {
    id: 6,
    name: 'PC-01',
    supervisor: {
      id: 3,
      name: 'Thiago Brandão',
      login: 'thiago.brandão',
      isActive: true
    },
    factory: {
      id: 3,
      name: 'VS'
    }
  }
} as unknown as Audit;
