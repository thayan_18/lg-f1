import assert from 'node:assert/strict';
import 'reflect-metadata';

import { beforeEach, describe, it } from 'node:test';
import Container from 'typedi';
import { BusinessErrorCode } from '../../commons/BusinessErrorCode';
import { UserLoginDTO } from '../../dto/UserLoginDTO';
import { LoginValidator } from '../../validator/LoginValidator';

const USER_LOGIN_MAX_LENGHT = 'tamanho maximo do campo deve ser igual a cinquenta caracteres';

describe('LoginValidator', () => {

  let loginValidator: LoginValidator;

  beforeEach(() => {
    loginValidator = Container.get(LoginValidator);
  });

  it('deve validar entrada da propriedade login', async () => {
    const loginInputMock = new UserLoginDTO();

    assert.throws(() => loginValidator.validate(loginInputMock), {
      code: BusinessErrorCode.USER.USER_LOGIN_REQUIRED.code,
      message: BusinessErrorCode.USER.USER_LOGIN_REQUIRED.message
    });

    loginInputMock.login = USER_LOGIN_MAX_LENGHT;
    assert.throws(() => loginValidator.validate(loginInputMock), {
      code: BusinessErrorCode.USER.USER_LOGIN_MAX_LENGHT.code,
      message: BusinessErrorCode.USER.USER_LOGIN_MAX_LENGHT.message
    });
  });

  it('deve validar entrada da propriedade password', async () => {
    const loginInputMock = new UserLoginDTO();
    loginInputMock.login = 'Admin';

    assert.throws(() => loginValidator.validate(loginInputMock), {
      code: BusinessErrorCode.USER.USER_PASSWORD_REQUIRED.code,
      message: BusinessErrorCode.USER.USER_PASSWORD_REQUIRED.message
    });

    loginInputMock.password = USER_LOGIN_MAX_LENGHT;
    assert.throws(() => loginValidator.validate(loginInputMock), {
      code: BusinessErrorCode.USER.USER_PASSWORD_MAX_LENGHT.code,
      message: BusinessErrorCode.USER.USER_PASSWORD_MAX_LENGHT.message
    });
  });
});
