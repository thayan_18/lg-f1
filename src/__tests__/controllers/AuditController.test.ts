import 'reflect-metadata';
import Container from 'typedi';
import { UserService } from '../../services/UserService';

import assert from 'node:assert/strict';
import { after, before, beforeEach, describe, it, mock } from 'node:test';
import { AuditRepository } from '../../repositorys/AuditRepository';
import { AuditService } from '../../services/AuditService';
import { auditObjMock1 } from '../common/AuditRepositoryMock';
import { auditDTOMock1, auditDTOMock2 } from '../common/AuditServiceMock';

const userIdMock = 0;
const auditRepositoryMock = {
  getAllCurrentUserAudits: null
} as unknown as AuditRepository;

const auditServiceMock = {
  getAllCurrentUserAudits: null
} as unknown as AuditService;

describe('AuditController', () => {
  let auditService: AuditService;

  before(async () => {
    Container.set(UserService, auditServiceMock);
    Container.set(AuditRepository, auditRepositoryMock);
  });

  after(async () => {
    Container.reset();
  });

  beforeEach(() => {
    auditService = Container.get(AuditService);
  });

  it('deve obter lista das auditorias de um usuario', async () => {
    auditServiceMock.getAllCurrentUserAudits = mock.fn(() => { return Promise.resolve([auditDTOMock1, auditDTOMock2]); });
    auditRepositoryMock.getAllCurrentUserAudits = mock.fn(() => { return Promise.resolve([auditObjMock1, auditObjMock1]); });

    const audits = await auditService.getAllCurrentUserAudits(userIdMock);
    assert.ok(audits);
    assert.equal(audits.length, 2);
  });
});
