import 'reflect-metadata';
import Container from 'typedi';
import { User } from '../../models/User';
import { UserService } from '../../services/UserService';

import assert from 'node:assert/strict';
import { after, before, beforeEach, describe, it, mock } from 'node:test';

// Cria um objeto mock para o UserRepository
const userServiceMock = {
  saveUser: mock.fn(() => { return Promise.resolve({}); }),
  getAllUsers: mock.fn(() => { return Promise.resolve([{}]); })
};

describe('UserController', () => {
  let userService: UserService;

  before(async () => {
    Container.set(UserService, userServiceMock);
  });

  after(async () => {
    Container.reset();
  });

  beforeEach(() => {
    userService = Container.get(UserService);
  });

  it('deve salvar usuarios', async () => {
    const userResult = await userService.saveUser(new User());
    assert.ok(userResult);
    assert.strictEqual(userServiceMock.saveUser.mock.calls.length, 1);
  });

  it('deve obter usuarios', async () => {
    const userResult = await userService.getAllUsers();
    assert.ok(userResult);
    assert.equal(userResult.length, 1);
    assert.strictEqual(userServiceMock.getAllUsers.mock.calls.length, 1);
  });
});
