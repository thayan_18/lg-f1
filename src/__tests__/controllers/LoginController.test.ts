import 'reflect-metadata';
import Container from 'typedi';
import { LoginService } from '../../services/LoginService';

import assert from 'node:assert/strict';
import { after, before, beforeEach, describe, it, mock } from 'node:test';
import { AuthorizationDTO } from '../../dto/AuthorizationDTO';
import { UserLoginDTO } from '../../dto/UserLoginDTO';


const responseMock = {
  accessToken: '1241078ed61a26af0ba054147255bd2b3e8aaa3bdcaeff9dc1b2a223b27df7e4',
  user: { id: 1, name: 'Administrador', login: 'Admin', }
} as AuthorizationDTO;

const loginServiceMock = {
  processarLogin: mock.fn((): Promise<AuthorizationDTO> => {
    return Promise.resolve(responseMock);
  })
};

describe('LoginController', () => {
  let loginService: LoginService;

  before(async () => {
    Container.set(LoginService, loginServiceMock);
  });

  after(async () => {
    Container.reset();
  });

  beforeEach(() => {
    loginService = Container.get(LoginService);
  });

  it('deve salvar usuarios', async () => {
    const userMock = new UserLoginDTO();
    userMock.login = 'Admin';
    userMock.password = 'Admin';

    const auth: AuthorizationDTO = await loginService.processarLogin(userMock);

    assert.ok(auth);
    assert.ok(auth.user);
    assert.ok(auth.accessToken);
    assert.strictEqual(loginServiceMock.processarLogin.mock.calls.length, 1);
  });
});
