import assert from 'node:assert/strict';
import { before, beforeEach, describe, it } from 'node:test';
import 'reflect-metadata';
import Container from 'typedi';
import { AppDataSource } from '../../dataSource-config';
import { User } from '../../models/User';
import { UserRepository } from '../../repositorys/UserRepository';


describe('UserRepository', async () => {
  let userRepository: UserRepository;

  before(async () => {
    await AppDataSource.initialize();
  });

  beforeEach(() => {
    userRepository = Container.get(UserRepository);
  });

  it('deve persistir usuario', async () => {
    const userMock = new User();
    userMock.name = 'Admin';
    userMock.login = 'Admin';
    userMock.isActive = false;

    const userResult = await userRepository.save(userMock);

    assert.ok(userResult);
    assert.ok(userResult.id);
    assert.equal(userResult.name, userMock.name);
    assert.equal(userResult.login, userMock.login);
  });

  it('deve obter usuarios', async () => {
    const users = await userRepository.getAll();
    assert.ok(users);
  });

  it('deve obter pelo login', async () => {
    const users = await userRepository.getUserByLogin('Admin');
    assert.ok(users);
  });
});
