import assert from 'node:assert/strict';
import { before, beforeEach, describe, it } from 'node:test';
import 'reflect-metadata';
import Container from 'typedi';
import { AppDataSource } from '../../dataSource-config';
import { initScriptLoad } from '../../dataSource-init-script';
import { AuditRepository } from '../../repositorys/AuditRepository';



describe('AuditRepository', async () => {

  let auditRepository: AuditRepository;

  before(async () => {
    process.env.DB_DROP_DATABASE = 'true';
    process.env.DB_INIT_SCRIPT = 'true';
    await AppDataSource.initialize();
  });

  beforeEach(() => {
    auditRepository = Container.get(AuditRepository);
  });

  it('deve obter lista de auditorias de usuario', async () => {
    await initScriptLoad();
    const userId = 2;
    const audis = await auditRepository.getAllCurrentUserAudits(userId);
    const qtdInsertScript = 6;
    assert.ok(audis);
    assert.equal(audis.length, qtdInsertScript);
  });
});
