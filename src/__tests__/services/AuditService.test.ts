import assert from 'node:assert';
import { after, before, beforeEach, describe, it, mock } from 'node:test';
import 'reflect-metadata';
import Container from 'typedi';
import { AuditRepository } from '../../repositorys/AuditRepository';
import { AuditService } from '../../services/AuditService';
import { auditObjMock1, auditObjMock2 } from '../common/AuditRepositoryMock';

const userIdMock = 0;
const auditRepositoryMock = {
  getAllCurrentUserAudits: null
} as unknown as AuditRepository;

describe('AuditService', () => {

  let auditService: AuditService;

  before(async () => {
    Container.set(AuditService, auditRepositoryMock);
  });

  after(async () => {
    Container.reset();
  });

  beforeEach(() => {
    auditService = Container.get(AuditService);
  });

  it('deve obter lista de auditorias de usuario vazia', async () => {
    auditRepositoryMock.getAllCurrentUserAudits = mock.fn(() => { return Promise.resolve([]); });
    const audis = await auditService.getAllCurrentUserAudits(userIdMock);
    assert.ok(audis);
  });

  it('deve obter lista de auditorias de usuario com dois item', async () => {
    auditRepositoryMock.getAllCurrentUserAudits = mock.fn(() => { return Promise.resolve([auditObjMock1, auditObjMock2]); });
    const audits = await auditService.getAllCurrentUserAudits(userIdMock);
    assert.ok(audits);
    assert.equal(audits.length, 2);
  });
});
