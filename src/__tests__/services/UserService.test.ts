import assert from 'node:assert/strict';
import 'reflect-metadata';
import Container from 'typedi';
import { User } from '../../models/User';
import { UserRepository } from '../../repositorys/UserRepository';
import { UserService } from '../../services/UserService';

import { after, before, beforeEach, describe, it, mock } from 'node:test';


// Mock user
const userMock = new User();
userMock.id = 1;
userMock.name = 'User 1';
userMock.login = 'Admin';

// Cria um objeto mock para o UserRepository
const userRepositoryMock = {
  save: mock.fn(() => { return Promise.resolve(userMock); }),
  getAll: mock.fn(() => { return Promise.resolve([userMock]); }),
  getUserByLogin: mock.fn(() => { return Promise.resolve(userMock); })
};

describe('UserService', () => {

  let userService: UserService;

  before(async () => {
    Container.set(UserRepository, userRepositoryMock);
  });

  after(async () => {
    Container.reset();
  });

  beforeEach(() => {
    userService = Container.get(UserService);
  });

  it('deve persistir usuario', async () => {
    const userResult = await userService.saveUser(userMock);
    assert.ok(userResult);
    assert.equal(userResult.id, userMock.id);
    assert.equal(userResult.name, userMock.name);
    assert.strictEqual(userRepositoryMock.save.mock.calls.length, 1);
  });

  it('deve obter usuários', async () => {
    const users = await userService.getAllUsers();
    assert.ok(users);
    assert.strictEqual(users.length, 1);
    assert.strictEqual(userRepositoryMock.getAll.mock.calls.length, 1);
  });

  it('deve obter usuário pelo login', async () => {
    const logimMock = 'Admin';
    const user = await userService.getUserByLogin(logimMock);
    assert.ok(user);
    assert.strictEqual(user.login, logimMock);
  });
});
