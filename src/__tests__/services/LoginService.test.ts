import assert from 'node:assert';
import { afterEach, beforeEach, describe, it, mock } from 'node:test';
import 'reflect-metadata';
import Container from 'typedi';
import { BusinessErrorCode } from '../../commons/BusinessErrorCode';
import { AuthorizationLgDTO } from '../../dto/AuthorizationLgDTO';
import { UserLoginDTO } from '../../dto/UserLoginDTO';
import { User } from '../../models/User';
import { UserRepository } from '../../repositorys/UserRepository';
import { LoginService } from '../../services/LoginService';

// Mock user
const userDatabaseMock = new User();
userDatabaseMock.id = 1;
userDatabaseMock.name = 'Admin';
userDatabaseMock.login = 'Admin';
userDatabaseMock.isActive = true;

const userLgResponseMock = {
  UserName: 'joao.nunes',
  IsAuthenticated: true
} as AuthorizationLgDTO;

const loginInputMock = {
  login: 'joao.nunes',
  password: '40manaus2022*'
} as UserLoginDTO;

const userRepositoryMock = {
  getUserByLogin: mock.fn(() => { return Promise.resolve(userDatabaseMock); })
} as unknown as UserRepository;

const connectTimeoutError = ():void => {
  throw new Error('Connect Timeout Error');
};

const fetchMock = (paramResponse: AuthorizationLgDTO): void => {
  mock.method(global, 'fetch', () => {
    return { json: (): Promise<AuthorizationLgDTO> => { return Promise.resolve(paramResponse); }, status: 200 };
  });
};

describe('LoginService', () => {
  let loginService: LoginService;

  beforeEach(async () => {
    Container.set(UserRepository, userRepositoryMock);
    loginService = Container.get(LoginService);
  });

  afterEach(async () => {
    Container.reset();
  });

  it('deve processar login admin com sucesso', async () => {
    const userMock = new UserLoginDTO();
    userMock.login = 'Admin';
    userMock.password = 'Admin';
    const objAuth = await loginService.processarLogin(userMock);
    assert.ok(objAuth);
  });

  it('deve processar login na base da lg com sucesso', async () => {
    fetchMock(userLgResponseMock);
    process.env.DISABLE_LOGIN_LG_API = 'false';
    const objAuth = await loginService.processarLogin(loginInputMock);
    assert.ok(objAuth);
  });

  it('deve processar login na base da lg com sucesso, porem nao autorizado', async () => {
    userLgResponseMock.IsAuthenticated = false;
    fetchMock(userLgResponseMock);

    assert.rejects(loginService.processarLogin(loginInputMock), {
      code: BusinessErrorCode.AUTHORIZATION.UNAUTHORIZED_LG.code,
      message: BusinessErrorCode.AUTHORIZATION.UNAUTHORIZED_LG.message
    });
  });

  it('deve processar login na base da lg, porem com erro de conexao', async () => {

    mock.method(global, 'fetch', () => {
      return { json: (): Promise<AuthorizationLgDTO> => { return Promise.reject(connectTimeoutError()); } };
    });

    assert.rejects(loginService.processarLogin(loginInputMock), {
      code: BusinessErrorCode.AUTHORIZATION.CONNECT_TIMEOUT_LG.code,
      message: BusinessErrorCode.AUTHORIZATION.CONNECT_TIMEOUT_LG.message
    });
  });

  it('deve processar login na base da lg com sucesso, porem usuario nao existe na base de dados do projeto', async () => {
    userLgResponseMock.IsAuthenticated = true;
    userRepositoryMock.getUserByLogin = mock.fn(() => { return Promise.resolve(null); });
    fetchMock(userLgResponseMock);

    assert.rejects(loginService.processarLogin(loginInputMock), {
      code: BusinessErrorCode.USER.USER_NOT_FOUND.code,
      message: BusinessErrorCode.USER.USER_NOT_FOUND.message
    });
  });

  it('deve processar login na base da lg com sucesso, porem usuario esta inativo', async () => {
    // mock
    userDatabaseMock.isActive = false;
    userRepositoryMock.getUserByLogin = mock.fn(() => { return Promise.resolve(userDatabaseMock); });
    mock.method(global, 'fetch', () => {
      userLgResponseMock.IsAuthenticated = true;
      return { json: (): Promise<AuthorizationLgDTO> => { return Promise.resolve(userLgResponseMock); }, status: 200 };
    });

    assert.rejects(loginService.processarLogin(loginInputMock), {
      code: BusinessErrorCode.USER.USER_IS_DISABLED.code, message: BusinessErrorCode.USER.USER_IS_DISABLED.message
    });
  });
});
