import * as dotenv from 'dotenv';

import Container from 'typedi';
import { LGOIN_USER_DEFAULT } from './commons/Constants';
import { AppDataSource } from './dataSource-config';
import { UserRepository } from './repositorys/UserRepository';

// Configuração do variável de ambiente
dotenv.config();

export async function initScriptLoad(): Promise<void> {
  await createUserDefault();

  if ((`${process.env.DB_INIT_SCRIPT || false}` === 'true')) {
    await createAudits();
  }
}

export async function createUserDefault(): Promise<void> {
  const userRepository: UserRepository = Container.get(UserRepository);
  const userExists = await userRepository.getUserByLogin(LGOIN_USER_DEFAULT);
  if (!userExists) {
    await AppDataSource.query('INSERT INTO users (name, login, isActive) VALUES(?,?,?)',
      [`${process.env.LGOIN_USER_DEFAULT || LGOIN_USER_DEFAULT}`, `${process.env.LGOIN_USER_DEFAULT || LGOIN_USER_DEFAULT}`, true]);

  }
}

export async function createAudits(): Promise<void> {

  // user
  await AppDataSource.query('INSERT INTO users (name, login, isActive) VALUES(?,?,?)', ['Richard Araujo', 'richard.araujo', true]);
  await AppDataSource.query('INSERT INTO users (name, login, isActive) VALUES(?,?,?)', ['Thiago Brandão', 'thiago.brandão', true]);

  // factory
  await AppDataSource.query('INSERT INTO factory (name) VALUES(?)', ['TV/AV']);
  await AppDataSource.query('INSERT INTO factory (name) VALUES(?)', ['H&A']);
  await AppDataSource.query('INSERT INTO factory (name) VALUES(?)', ['VS']);

  // line/areas
  await AppDataSource.query('INSERT INTO line_area (name, supervisorId, factoryId) VALUES(?,?,?)', ['TV-01', 3, 1]);
  await AppDataSource.query('INSERT INTO line_area (name, supervisorId, factoryId) VALUES(?,?,?)', ['TV-02', 3, 2]);
  await AppDataSource.query('INSERT INTO line_area (name, supervisorId, factoryId) VALUES(?,?,?)', ['TV-03', 3, 3]);
  await AppDataSource.query('INSERT INTO line_area (name, supervisorId, factoryId) VALUES(?,?,?)', ['TV-04', 3, 1]);
  await AppDataSource.query('INSERT INTO line_area (name, supervisorId, factoryId) VALUES(?,?,?)', ['TV-05', 3, 2]);
  await AppDataSource.query('INSERT INTO line_area (name, supervisorId, factoryId) VALUES(?,?,?)', ['PC-01', 3, 3]);

  // subsidiary
  await AppDataSource.query('INSERT INTO subsidiary (name) VALUES(?)', ['Manuas']);

  // factory_improvements
  await AppDataSource.query('INSERT INTO factory_improvements (name) VALUES(?)', ['FI-11']);

  // Audit
  await AppDataSource.query(`INSERT INTO audit  (creationDate, startDate, endDate, status, subsidiaryId, factoryImprovementsId, lineAreaId, userAuditorId)
                             VALUES(?, ?, ?, ?, ?, ?, ?, ?);`, [new Date(), new Date(), new Date(), 'TODO', 1, 1, 1, 2]);
  await AppDataSource.query(`INSERT INTO audit  (creationDate, startDate, endDate, status, subsidiaryId, factoryImprovementsId, lineAreaId, userAuditorId)
                             VALUES(?, ?, ?, ?, ?, ?, ?, ?);`, [new Date(), new Date(), new Date(), 'TODO', 1, 1, 2, 2]);
  await AppDataSource.query(`INSERT INTO audit  (creationDate, startDate, endDate, status, subsidiaryId, factoryImprovementsId, lineAreaId, userAuditorId)
                             VALUES(?, ?, ?, ?, ?, ?, ?, ?);`, [new Date(), new Date(), new Date(), 'TODO', 1, 1, 3, 2]);
  await AppDataSource.query(`INSERT INTO audit  (creationDate, startDate, endDate, status, subsidiaryId, factoryImprovementsId, lineAreaId, userAuditorId)
                             VALUES(?, ?, ?, ?, ?, ?, ?, ?);`, [new Date(), new Date(), new Date(), 'TODO', 1, 1, 4, 2]);
  await AppDataSource.query(`INSERT INTO audit  (creationDate, startDate, endDate, status, subsidiaryId, factoryImprovementsId, lineAreaId, userAuditorId)
                             VALUES(?, ?, ?, ?, ?, ?, ?, ?);`, [new Date(), new Date(), new Date(), 'TODO', 1, 1, 5, 2]);
  await AppDataSource.query(`INSERT INTO audit  (creationDate, startDate, endDate, status, subsidiaryId, factoryImprovementsId, lineAreaId, userAuditorId)
                             VALUES(?, ?, ?, ?, ?, ?, ?, ?);`, [new Date(), new Date(), new Date(), 'COMPLETED', 1, 1, 6, 2]);
}
