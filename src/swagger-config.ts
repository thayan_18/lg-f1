import swaggerJSDoc, { SwaggerDefinition } from 'swagger-jsdoc';
import { ROUTER_PREFIX_URL } from './commons/Constants';


const swaggerDefinition: SwaggerDefinition = {
  info: {
    title: 'API',
    version: '3.0.0',
    description: 'Documentação da API'
  },
  basePath: ROUTER_PREFIX_URL,
  securityDefinitions: {
    Auth: {
      type: 'apiKey',
      name: 'Authorization',
      in: 'header'
    }
  },
  security: [
    {
      Auth: []
    }
  ]
};

const options: swaggerJSDoc.Options = {
  swaggerDefinition,
  apis: ['src/**/*.ts']
};

const setupSwagger = swaggerJSDoc(options);

export default setupSwagger;
