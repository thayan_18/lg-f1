import * as dotenv from 'dotenv';
import { DataSource, DataSourceOptions } from 'typeorm';
import { User } from './models/User';
import { Audit } from './models/Audit';
import { Factory } from './models/Factory';
import { FactoryImprovements } from './models/FactoryImprovements';
import { LineArea } from './models/LineArea';
import { Subsidiary } from './models/Subsidiary';

// Configuração do variável de ambiente
dotenv.config();

interface DataSourceInterface {
  development: DataSourceOptions,
  test: DataSourceOptions
}

const dataBaseConf: DataSourceInterface = {
  development: {
    name: 'AppDataSource',
    type: 'mariadb',
    host: process.env.DB_HOST || 'localhost',
    port: parseInt(JSON.stringify(process.env.DB_PORT || '3306')),
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    entities: [User, Audit, Factory, FactoryImprovements, LineArea, Subsidiary],
    synchronize: true,
    dropSchema: `${process.env.DB_DROP_DATABASE || false}` === 'true',
    logging: false
  },
  test: {
    name: 'AppDataSourceTest',
    type: 'sqlite',
    database: ':memory:',
    entities: ['src/models/*.ts'],
    dropSchema: true,
    synchronize: true,
    logging: false
  }
};

const dataSource = process.env.NODE_ENV === 'test' ? dataBaseConf.test : dataBaseConf.development;

export const AppDataSource = new DataSource(dataSource);
