import { AuditDTO } from '../dto/AuditDTO';
import { FactoryDTO } from '../dto/FactoryDTO';
import { UserDTO } from '../dto/UserDTO';
import { Audit } from '../models/Audit';

export class AuditMapper {

  public static toListDTO(sourceArray: Array<Audit>): Array<AuditDTO> {
    const targetArray: Array<AuditDTO> = [];

    sourceArray.forEach((item: Audit) => {
      const dto = new AuditDTO();

      dto.id = item.id;
      dto.status = item.status.toString();

      dto.userAuditor = new UserDTO();
      dto.userAuditor.id = item.userAuditor.id;
      dto.userAuditor.name = item.userAuditor.name;

      dto.userSupervisorLineArea = new UserDTO();
      dto.userSupervisorLineArea.id = item.lineArea.supervisor.id;
      dto.userSupervisorLineArea.name = item.lineArea.supervisor.name;

      dto.factory = new FactoryDTO();
      dto.factory.id = item.lineArea.factory.id;
      dto.factory.name = item.lineArea.factory.name;

      targetArray.push(dto);
    });
    return targetArray;
  }
}
