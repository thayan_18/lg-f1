import * as dotenv from 'dotenv';
import jwt, { JwtPayload } from 'jsonwebtoken';
import { Inject, Service } from 'typedi';
import { BusinessErrorCode } from '../commons/BusinessErrorCode';
import { JWT_EXPIRES_IN, JWT_SECRET, LGOIN_USER_DEFAULT, PASSWORD_USER_DEFAULT } from '../commons/Constants';
import { AuthorizationDTO } from '../dto/AuthorizationDTO';
import { AuthorizationLgDTO } from '../dto/AuthorizationLgDTO';
import { UserLoginDTO } from '../dto/UserLoginDTO';
import { BusinessErrorAuthorizationException } from '../exceptions/BusinessErrorAuthorizationException';
import { BusinessErrorException } from '../exceptions/BusinessErrorException';
import { User } from '../models/User';
import { LoginValidator } from '../validator/LoginValidator';
import { UserService } from './UserService';

dotenv.config();

@Service()
export class LoginService {

  constructor(@Inject() public userService: UserService, @Inject() public loginValidator: LoginValidator) { }


  async processarLogin(credencias: UserLoginDTO): Promise<AuthorizationDTO> {

    // valida a entrada das credencias
    this.loginValidator.validate(credencias);

    // verificar se 'e usuario default
    const isDefaultUser: boolean = this.processarUserDefault(credencias);

    if (!isDefaultUser) {
      // valida as credenciais na API LG
      const responserApiLG: boolean = await this.processarLoginApiLG(credencias);

      if (!responserApiLG) {
        throw new BusinessErrorAuthorizationException(BusinessErrorCode.AUTHORIZATION.UNAUTHORIZED_LG.code,
          BusinessErrorCode.AUTHORIZATION.UNAUTHORIZED_LG.message);
      }
    }

    // depois que o usuário foi validado na api lg, ele deve ser recuperado do banco de dados
    const userBase: User | null = await this.userService.getUserByLogin(credencias.login);

    if (!userBase) {
      throw new BusinessErrorException(BusinessErrorCode.USER.USER_NOT_FOUND.code, BusinessErrorCode.USER.USER_NOT_FOUND.message);
    }

    if (!userBase.isActive) {
      throw new BusinessErrorAuthorizationException(BusinessErrorCode.USER.USER_IS_DISABLED.code,
        BusinessErrorCode.USER.USER_IS_DISABLED.message);
    }

    return this.authorizationCryptographyEncoder(userBase);
  }


  protected async processarLoginApiLG(userLoginParam: UserLoginDTO): Promise<boolean> {
    try {
      const disableLoginWithLgApi = JSON.parse(`${process.env.DISABLE_LOGIN_LG_API || false}`);
      const urlBaseApiLG = `${process.env.HOST_LG_API}`;
      const utlParamQuery = `userId=${userLoginParam.login}&password=${userLoginParam.password}`;

      let responseBoolean = disableLoginWithLgApi;

      if (!disableLoginWithLgApi) {
        const urlLg = `${urlBaseApiLG}?${utlParamQuery}`;
        console.log('[URL Api LG]:', urlLg);
        const responseRequest: Response = await fetch(urlLg); // request for lg api
        const data: AuthorizationLgDTO = await responseRequest.json() as AuthorizationLgDTO;

        responseBoolean = data.IsAuthenticated;
      }
      return responseBoolean;
    } catch (error) {
      console.log('[Erro Api LG]: ', error);
      throw new BusinessErrorAuthorizationException(BusinessErrorCode.AUTHORIZATION.CONNECT_TIMEOUT_LG.code,
        BusinessErrorCode.AUTHORIZATION.CONNECT_TIMEOUT_LG.message);
    }
  }

  protected authorizationCryptographyEncoder(userBase: User): AuthorizationDTO {

    const JWT_SECRET_AUX = `${process.env.JWT_SECRET || JWT_SECRET}`;
    const authorization: AuthorizationDTO = new AuthorizationDTO();

    try {
      // to generate
      authorization.user = userBase;
      authorization.accessToken = jwt.sign(
        { id: userBase.id, login: userBase.login },
        JWT_SECRET_AUX, { expiresIn: process.env.JWT_EXPIRES_IN || JWT_EXPIRES_IN }
      );

      // verify
      const payload = jwt.verify(authorization.accessToken, JWT_SECRET_AUX) as JwtPayload;
      authorization.createdIn = `${payload.iat}`;
      authorization.expiresIn = `${payload.exp}`;

      return authorization;
    } catch (error) {
      throw new BusinessErrorAuthorizationException(BusinessErrorCode.AUTHORIZATION.TOKEN_ACCESS.code,
        BusinessErrorCode.AUTHORIZATION.TOKEN_ACCESS.message);
    }
  }

  protected processarUserDefault(credencias: UserLoginDTO): boolean {
    const passwordDefault = `${process.env.PASSWORD_USER_DEFAULT || PASSWORD_USER_DEFAULT}`;
    const passwordDecoder = Buffer.from(passwordDefault, 'base64').toString('ascii');

    let isAuthenticated = false;

    if (credencias.login === LGOIN_USER_DEFAULT) {
      isAuthenticated = credencias.password === passwordDecoder;

      if (!isAuthenticated) {
        throw new BusinessErrorAuthorizationException(BusinessErrorCode.AUTHORIZATION.INVALID_LOGIN.code,
          BusinessErrorCode.AUTHORIZATION.INVALID_LOGIN.message);
      }

    }

    return isAuthenticated;
  }
}
