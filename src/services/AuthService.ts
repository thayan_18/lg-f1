import { Service } from 'typedi';
import { UserDTO } from '../dto/UserDTO';
import { tokenAccessUserCurrent } from '../middlewares/CustomAuthorizationMiddeware';


@Service()
export class AuthService {
  public getSessionUser(): UserDTO {
    const userCurrent = new UserDTO();
    userCurrent.id = tokenAccessUserCurrent.data.id;
    userCurrent.login = tokenAccessUserCurrent.data.login;
    return userCurrent;
  }
}
