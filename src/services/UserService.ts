import { Inject, Service } from 'typedi';
import { BusinessErrorCode } from '../commons/BusinessErrorCode';
import { BusinessErrorException } from '../exceptions/BusinessErrorException';
import { User } from '../models/User';
import { UserRepository } from '../repositorys/UserRepository';


@Service()
export class UserService {

  constructor(@Inject() private readonly userRepository: UserRepository) { }

  async saveUser(user: User): Promise<User> {
    if (!user.name) {
      throw new BusinessErrorException(BusinessErrorCode.USER.USER_NAME_REQUIRED.code,
        BusinessErrorCode.USER.USER_NAME_REQUIRED.message);
    }
    return this.userRepository.save(user);
  }

  async getAllUsers(): Promise<Array<User>> {
    return await this.userRepository.getAll();
  }

  async getUserByLogin(login: string): Promise<User | null> {
    return await this.userRepository.getUserByLogin(login);
  }
}
