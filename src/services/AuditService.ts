import 'reflect-metadata';
import { Inject, Service } from 'typedi';
import { AuditDTO } from '../dto/AuditDTO';
import { AuditMapper } from '../mapper/AuditMapper';
import { AuditRepository } from '../repositorys/AuditRepository';


@Service()
export class AuditService {

  constructor(@Inject() private readonly auditRepository: AuditRepository) { }

  public async getAllCurrentUserAudits(userId: number): Promise<Array<AuditDTO>> {
    const audits = await this.auditRepository.getAllCurrentUserAudits(userId);
    return Promise.resolve(AuditMapper.toListDTO(audits));
  }
}
