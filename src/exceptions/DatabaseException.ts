export class DatabaseException extends Error {
  constructor(public message: string) {
    super(message);
  }
}
