import express from 'express';
import Container from 'typedi';
import { UserController } from '../controllers/UserController';

const userRouter = express.Router();
const controller = Container.get(UserController);

// Rota para buscar todos os usuários
userRouter.get('/', controller.getAllUsers.bind(controller));

// Rota para criar um usuário
userRouter.post('/', controller.saveUser.bind(controller));


// ex
// // Rota para buscar um usuário específico
// router.get('/users/:id', userController.getUser.bind(userController));

// // Rota para atualizar um usuário
// router.put('/users/:id', userController.updateUser.bind(userController));

// // Rota para excluir um usuário
// router.delete('/users/:id', userController.deleteUser.bind(userController));

export default userRouter;
