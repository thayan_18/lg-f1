import express from 'express';
import Container from 'typedi';
import { AuditController } from '../controllers/AuditController';

const auditRouter = express.Router();
const controller = Container.get(AuditController);

// Rota para buscar todos os usuários
auditRouter.get('/get-user-audits', controller.getAllUserAudits.bind(controller));

export default auditRouter;
