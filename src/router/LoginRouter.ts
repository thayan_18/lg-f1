import express from 'express';
import Container from 'typedi';
import { LoginController } from '../controllers/LoginController';

const loginRouter = express.Router();
const controller = Container.get(LoginController);

loginRouter.post('/', controller.login.bind(controller));

export default loginRouter;
