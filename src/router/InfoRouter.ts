import express from 'express';
import Container from 'typedi';
import { InfoController } from '../controllers/InfoController';

const infoRouter = express.Router();
const controller = Container.get(InfoController);
infoRouter.get('/', controller.getPackageInfo.bind(controller));

export default infoRouter;
