import { NextFunction, Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import { BusinessErrorCode } from '../commons/BusinessErrorCode';
import { HEADER_AUTHORIZATION } from '../commons/Constants';
import { UserDTO } from '../dto/UserDTO';
import { BusinessErrorAuthorizationException } from '../exceptions/BusinessErrorAuthorizationException';


export const tokenAccessUserCurrent = {
  data: {} as UserDTO
};

export class CustomAuthorizationMiddeware {
  public static handler(request: Request, response: Response, next: NextFunction): void {
    try {
      const token = request.headers[HEADER_AUTHORIZATION];
      if (token) {
        const jwtToken: any = jwt.verify(token, `${process.env.JWT_SECRET}`);
        tokenAccessUserCurrent.data.id = jwtToken.id;
        tokenAccessUserCurrent.data.login = jwtToken.login;
      } else {
        throw Error(BusinessErrorCode.AUTHORIZATION.UNAUTHORIZED.message);
      }
      next();
    } catch (error) {
      throw new BusinessErrorAuthorizationException(BusinessErrorCode.AUTHORIZATION.UNAUTHORIZED.code,
        BusinessErrorCode.AUTHORIZATION.UNAUTHORIZED.message);
    }
  }
}
