import { NextFunction, Request, Response } from 'express';
import { BusinessErrorAuthorizationException } from '../exceptions/BusinessErrorAuthorizationException';
import { BusinessErrorException } from '../exceptions/BusinessErrorException';


const HTTP_CODE_UNAUTHORIZED = 401;
const HTTP_CODE_BAD_REQUEST = 400;
const HTTP_CODE_INTERNAL_SERVER_ERRO = 500;

interface BusinessErrorCode {
  code: string,
  message: string
}

export class CustomErrorHandlerMiddewares {
  public static handler(error: Error, request: Request, response: Response, next: NextFunction): void {
    // Exemplo de manipulação de erros específicos
    const businessErrorCode: BusinessErrorCode = {} as BusinessErrorCode;

    if (error instanceof BusinessErrorException) {
      businessErrorCode.code = error.code;
      businessErrorCode.message = error.message;
      response.status(HTTP_CODE_BAD_REQUEST).json(businessErrorCode);
    } else if (error instanceof BusinessErrorAuthorizationException) {
      businessErrorCode.code = error.code;
      businessErrorCode.message = error.message;
      response.status(HTTP_CODE_UNAUTHORIZED).json(businessErrorCode);
    } else {
      businessErrorCode.code = HTTP_CODE_INTERNAL_SERVER_ERRO.toString();
      businessErrorCode.message = error.toString();
      response.status(HTTP_CODE_INTERNAL_SERVER_ERRO).json(businessErrorCode);
    }

    next(error);
  }
}
