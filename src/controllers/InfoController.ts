import * as dotenv from 'dotenv';
import { Request, Response } from 'express';
import 'reflect-metadata';
import { Service } from 'typedi';


// Configuração do variável de ambiente
dotenv.config();

@Service()
export class InfoController {


  /**
   * @swagger
   * /info:
   *   get:
   *     tags: [Info]
   *     responses:
   *       200:
   *         description: Sucesso
   */
  async getPackageInfo(req: Request, res: Response): Promise<void> {
    res.json({
      name: `${process.env.npm_package_name}`.toUpperCase(),
      version: `${process.env.npm_package_version}`
    });
  }
}
