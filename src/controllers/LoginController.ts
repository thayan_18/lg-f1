import { Request, Response } from 'express';
import { Inject, Service } from 'typedi';
import { AuthorizationDTO } from '../dto/AuthorizationDTO';
import { UserLoginDTO } from '../dto/UserLoginDTO';
import { LoginService } from '../services/LoginService';


@Service()
export class LoginController {

  constructor(@Inject() public loginService: LoginService) { }

  /**
   * @swagger
   *
   * /login:
   *   post:
   *     description: Responsável pela autenticação e criação do usuário.
   *     tags: [Login]
   *     produces:
   *       - application/json
   *     consumes:
   *       - application/json
   *     parameters:
   *       - in: body
   *         name: Login
   *         schema:
   *           type: object
   *           properties:
   *            login:
   *              type: string
   *            password:
   *              type: string
   *           required:
   *             - login
   *             - password
   *     responses:
   *       200:
   *         description: Success
   *       400:
   *         description: Bad Request
   *       401:
   *         description: Not authenticated
   */
  async login(req: Request, res: Response): Promise<void> {
    const userLogin: UserLoginDTO = req.body;
    const response: AuthorizationDTO = await this.loginService.processarLogin(userLogin);
    res.json(response);
  }
}
