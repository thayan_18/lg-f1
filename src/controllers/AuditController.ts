import { Request, Response } from 'express';
import 'reflect-metadata';
import { Inject, Service } from 'typedi';
import { AuditService } from '../services/AuditService';
import { AuthService } from '../services/AuthService';


@Service()
export class AuditController {

  constructor(@Inject() private auditService: AuditService, private authService: AuthService) { }

  /**
  * @swagger
  * /audit/get-user-audits:
  *   get:
  *     tags: [Audit]
  *     responses:
  *       200:
  *         description: Sucesso
  *       401:
  *         description: Not authenticated
  */
  public async getAllUserAudits(req: Request, res: Response): Promise<void> {
    const userSesseion = this.authService.getSessionUser();
    res.json(await this.auditService.getAllCurrentUserAudits(userSesseion.id));
  }
}
