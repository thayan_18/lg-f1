import { Request, Response } from 'express';
import 'reflect-metadata';
import { Inject, Service } from 'typedi';
import { User } from '../models/User';
import { UserService } from '../services/UserService';


@Service()
export class UserController {

  constructor(@Inject() public userService: UserService) { }

  /**
   * @swagger
   * /users:
   *   get:
   *     tags: [Users]
   *     responses:
   *       200:
   *         description: Sucesso
   *       401:
   *         description: Not authenticated
   */
  public async getAllUsers(req: Request, res: Response): Promise<void> {
    const users = await this.userService.getAllUsers();
    res.json(users);
  }

  /**
   * @swagger
   *
   * /users:
   *   post:
   *     tags: [Users]
   *     produces:
   *       - application/json
   *     consumes:
   *       - application/json
   *     parameters:
   *       - in: body
   *         name: user
   *         schema:
   *           type: object
   *           properties:
   *            name:
   *              type: string
   *           required:
   *             - name
   *     responses:
   *       201:
   *         description: Created
   *       400:
   *         description: Bad Request
   */

  public async saveUser(req: Request, res: Response): Promise<void> {
    const user: User = req.body;
    const userNew = await this.userService.saveUser(user);
    res.json(userNew);
  }
}
