export enum StatusAuditEnum {
  TODO = 'TODO',
  IN_PROGRES = 'IN_PROGRES',
  COMPLETED = 'COMPLETED'
}
