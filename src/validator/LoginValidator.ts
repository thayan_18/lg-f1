import { Service } from 'typedi';
import { BusinessErrorCode } from '../commons/BusinessErrorCode';
import { UserLoginDTO } from '../dto/UserLoginDTO';
import { BusinessErrorException } from '../exceptions/BusinessErrorException';

@Service()
export class LoginValidator {

  public validate(userLogin: UserLoginDTO): void {
    this.validateLoginAuthentication(userLogin);
    this.validatePasswordAuthentication(userLogin);
  }

  protected validateLoginAuthentication(userLogin: UserLoginDTO): void {
    if ((userLogin.login === '' || userLogin.login === null || userLogin.login === undefined) ||
      (userLogin.login.trim().length === 0)) {
      throw new BusinessErrorException(BusinessErrorCode.USER.USER_LOGIN_REQUIRED.code,
        BusinessErrorCode.USER.USER_LOGIN_REQUIRED.message);
    }

    if (userLogin.login.length > 50) {
      throw new BusinessErrorException(BusinessErrorCode.USER.USER_LOGIN_MAX_LENGHT.code,
        BusinessErrorCode.USER.USER_LOGIN_MAX_LENGHT.message);
    }
  }

  protected validatePasswordAuthentication(userLogin: UserLoginDTO): void {
    if ((userLogin.password === '' || userLogin.password === null || userLogin.password === undefined) ||
      (userLogin.password.trim().length === 0)) {
      throw new BusinessErrorException(BusinessErrorCode.USER.USER_PASSWORD_REQUIRED.code,
        BusinessErrorCode.USER.USER_PASSWORD_REQUIRED.message);
    }

    if (userLogin.password.length > 50) {
      throw new BusinessErrorException(BusinessErrorCode.USER.USER_PASSWORD_MAX_LENGHT.code,
        BusinessErrorCode.USER.USER_PASSWORD_MAX_LENGHT.message);
    }
  }
}
