import { Service } from 'typedi';
import { QueryFailedError, Repository } from 'typeorm';
import { AppDataSource } from '../dataSource-config';
import { DatabaseException } from '../exceptions/DatabaseException';
import { User } from '../models/User';


@Service()
export class UserRepository {

  private repository: Repository<User>;

  constructor() {
    this.repository = AppDataSource.getRepository(User);
  }

  async save(user: User): Promise<User> {
    try {
      return await this.repository.save(user);
    } catch (error) {
      const knownError = error as QueryFailedError;
      throw new DatabaseException(knownError.driverError);
    }
  }

  async getAll(): Promise<Array<User>> {
    return await this.repository.find();
  }

  async getUserByLogin(login: string): Promise<User | null> {
    return await this.repository.findOne({ where: { login: login } });
  }
}
