import { Service } from 'typedi';
import { Repository } from 'typeorm';
import { AppDataSource } from '../dataSource-config';
import { Audit } from '../models/Audit';


@Service()
export class AuditRepository {

  private repository: Repository<Audit>;

  constructor() {
    this.repository = AppDataSource.getRepository(Audit);
  }

  public async getAllCurrentUserAudits(userId: number): Promise<Array<Audit>> {
    return await this.repository.find({
      relations: { userAuditor: true, lineArea: true },
      where: { userAuditor: { id: userId } }
    });
  }
}
