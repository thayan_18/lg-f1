export const BusinessErrorCode = {
  //range 1-10
  AUTHORIZATION: {
    UNAUTHORIZED: {
      code: '001',
      message: 'Erro 401 Não Autorizado.',
    },
    TOKEN_ACCESS: {
      code: '002',
      message: 'Erro 401 na geração do token de acesso.'
    },
    UNAUTHORIZED_LG: {
      code: '003',
      message: 'Erro 401 login inválido, não autorizado pela api lg.'
    },
    CONNECT_TIMEOUT_LG: {
      code: '004',
      message: 'Ocorreu erro de timeout com api de login lg'
    },
    INVALID_LOGIN: {
      code: '005',
      message: 'Erro 401 login inválido.'
    },
  },

  // range 10-30
  USER: {
    USER_NAME_REQUIRED: {
      code: '010',
      message: 'Nome do usuário é obrigatório.'
    },
    USER_LOGIN_REQUIRED: {
      code: '011',
      message: 'O login do usuário é obrigatório.'
    },
    USER_PASSWORD_REQUIRED: {
      code: '012',
      message: 'A Senha do usuário é obrigatória.'
    },
    USER_LOGIN_MAX_LENGHT: {
      code: '013',
      message: 'O login deve ser menor ou igual a 50 caracteres'
    },
    USER_PASSWORD_MAX_LENGHT: {
      code: '014',
      message: 'A senha deve ser menor ou igual a 50 caracteres'
    },
    USER_ALREADY_EXISTS: {
      code: '015',
      message: 'Já existe usuário com esse login, entre em contato com administrador.'
    },
    USER_NOT_FOUND: {
      code: '016',
      message: 'Usuário não encontrado, entre em contato com administrador para mais informações.'
    },
    USER_IS_DISABLED: {
      code: '017',
      message: 'Usuário está desativado, entre em contato com administrador para mais informações.'
    }
  },
};
