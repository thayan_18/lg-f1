export const LGOIN_USER_DEFAULT = 'Admin';
export const PASSWORD_USER_DEFAULT = 'QWRtaW4=';

export const ROUTER_PREFIX_URL = '/api/v1';

export const JWT_TYPE_TOKEN = 'Bearer';
export const JWT_EXPIRES_IN = '4h';
export const JWT_SECRET = 'PROJETO_LG_FI_11_2023';

export const HEADER_AUTHORIZATION = 'authorization';
