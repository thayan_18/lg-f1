import cors from 'cors';
import * as dotenv from 'dotenv';
import express, { NextFunction } from 'express';
import 'express-async-errors';
import 'reflect-metadata';
import swaggerUi from 'swagger-ui-express';
import setupSwagger from './swagger-config';

import bodyParser from 'body-parser';
import { Request, Response } from 'express';
import { ROUTER_PREFIX_URL } from './commons/Constants';
import { AppDataSource } from './dataSource-config';
import { initScriptLoad } from './dataSource-init-script';
import { CustomAuthorizationMiddeware } from './middlewares/CustomAuthorizationMiddeware';
import { CustomErrorHandlerMiddewares } from './middlewares/CustomErrorHandlerMiddewares';
import auditRouter from './router/AuditRouter';
import infoRouter from './router/InfoRouter';
import loginRouter from './router/LoginRouter';
import userRouter from './router/UserRouter';

// Configuração do variável de ambiente
dotenv.config();

// inicialização banco de dados
AppDataSource.initialize().then(() => {
  console.log('A banco de dados foi inicializado!');

  // script sql
  initScriptLoad();

}).catch((err) => {
  console.error('Erro durante a inicialização do banco de dados', err);
});


// server
const app = express();

// conf
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// api swagger
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(setupSwagger));

// login public
app.use(`${ROUTER_PREFIX_URL}/login`, loginRouter);

// authorization checked
app.use((req: Request, res: Response, next: NextFunction) => {
  CustomAuthorizationMiddeware.handler(req, res, next);
});

// router protected
app.use(`${ROUTER_PREFIX_URL}/users`, userRouter);
app.use(`${ROUTER_PREFIX_URL}/audit`, auditRouter);
app.use(`${ROUTER_PREFIX_URL}/info`, infoRouter);

// Error handle
app.use((error: Error, req: Request, res: Response, next: NextFunction) => {
  CustomErrorHandlerMiddewares.handler(error, req, res, next);
});

const appPort = process.env.PORT || 3000;
app.listen(appPort, () => {
  console.log(`Servidor rodando em http://localhost:${appPort}`);
  console.log(`Servidor api-swagger rodando em http://localhost:${appPort}/api-docs/#/`);
});
